# Expose and Paper Template

Version: 0.1  
Copyright (C) 2016 [Nicolas Navarro-Guerrero](https://nicolas-navarro-guerrero.github.io)  
Contact:  nicolas.navarro.guerrero@gmail.com  

        https://github.com/nicolas-navarro-guerrero  
        https://bitbucket.org/nicolas-navarro-guerrero  
        https://nicolas-navarro-guerrero.github.io  

*******************************************************************************
## Overview

*******************************************************************************
### File structure:

*******************************************************************************
### Usage
* Main file `proposal.tex`  

* Complete the file `document-metadata.tex` with your personal information  

* the fancy ToDo list can be disabled on the final version by simple uncommenting `disable` in the declaration on the `todonotes` package at the end of file `document-formatting.tex`  

#### Configuring [Texmaker](http://www.xm1math.net/texmaker/)  
Go to `options` -> `Configure Texmaker` -> `commands`  

* Check `use a build subdirectory for output files`  
    
* change from `bibtex %` -> `biber build/%`  

* change from `makeindex %.idx` -> `makeindex build/%.idx`  

Go to `user` -> `user commands` -> `edit user commands` (optional)  

* add `makeglossaries -d build %`  

Compile using pdflatex, bibtex, makeindex (optional), makeglossaries (optional)

*******************************************************************************
### Dependencies:
Tested on Ubuntu `14.04`, biber `1.8.1`, texlive `2013.20140215`  

*******************************************************************************
## LICENSE FOR DOCUMENTATION AND FIGURES:
The collection of source files and the images generated directly from them are work by Nicolás Navarro-Guerrero and are licensed under the Creative Commons Attribution-ShareAlike 4.0 International (CC BY-SA 4.0). To view a copy of this license, visit https://creativecommons.org/licenses/by-sa/4.0/

*******************************************************************************
## LICENSE FOR TEX FILE:
This collection of scripts, tex and source files are part of 
"yet another thesis template" 
and are free software: you can redistribute it and/or modify it under the terms 
of the GNU General Public License as published by the Free Software Foundation, 
either version 3 of the License, or (at your option) any later version. 

"yet another thesis template" 
is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; 
without even the implied warranty of MERCHANTABILITY or FITNESS FOR A 
PARTICULAR PURPOSE. See the GNU General Public License for more details. 

You should have received a copy of the GNU General Public License along with 
"yet another thesis template" 
If not, see https://www.gnu.org/licenses/. 
